//
//  PromoCell.swift
//  Uciapp
//
//  Created by Mario De luca on 11/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit

protocol SelectionItemProtocol: class {
    func selecteditem()
}

class PromoCell: UITableViewCell {
    
    var images = [UIImage(named: "poster"),UIImage(named: "promostar"),UIImage(named: "jumanji-1"),UIImage(named: "promo1")]
    
    weak var delegate: SelectionItemProtocol?
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.delegate = self as UICollectionViewDelegate
        collectionView.dataSource = self as UICollectionViewDataSource
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension PromoCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        cell.images.image = images[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.selecteditem()
    }
    
    //funzione che setta la size della cella
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 414, height: 360)
    }
    
    
    
    
}

