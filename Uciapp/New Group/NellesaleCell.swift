//
//  NellesaleCell.swift
//  Uciapp
//
//  Created by Mario De luca on 11/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit

class NellesaleCell: UITableViewCell  {
    
    var images2 = [UIImage(named: "poster1"),UIImage(named: "poster2"),UIImage(named: "poster3"),UIImage(named: "poster7"),UIImage(named: "poster8"),UIImage(named: "poster12")]

    @IBOutlet weak var collectionView2: UICollectionView!
     override func awakeFromNib() {
       super.awakeFromNib()
           // Initialization code
           
       collectionView2.delegate = self as UICollectionViewDelegate
       collectionView2.dataSource = self as UICollectionViewDataSource
                      }

                      override func setSelected(_ selected: Bool, animated: Bool) {
                      super.setSelected(selected, animated: animated)

                      // Configure the view for the selected state
                      }

                      }

extension NellesaleCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell2 = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! CollectionViewCell2
        cell2.images2.image = images2[indexPath.row]
        
        return cell2
    }
//
//                      func collectionView2(_ collectionView2: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//                          return images2.count
//                      }
              

//                          func collectionView2(_ collectionView2: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//                              let cell2 = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! CollectionViewCell2
//                              cell2.images2.image = images2[indexPath.row]
//
//                              return cell2
//
//                          }
                          
                          
                          //funzione che setta la size della cella
         func collectionView(_ collectionView2: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                              return CGSize(width: 141, height: 235)
                          }
                          


}
