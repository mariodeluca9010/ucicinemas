//
//  CollectionViewCell2.swift
//  Uciapp
//
//  Created by Mario De luca on 11/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit

class CollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var images2: UIImageView!
    
    var image2 = [UIImage(named: "poster1"),UIImage(named: "poster2"),UIImage(named: "poster3"),UIImage(named: "poster7"),UIImage(named: "poster8"),UIImage(named: "poster12")]

    override func awakeFromNib() {
        super.awakeFromNib()
    
}
}
