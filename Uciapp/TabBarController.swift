//
//  TabBarController.swift
//  Uciapp
//
//  Created by Mario De luca on 12/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // make unselected icons white
        self.tabBar.unselectedItemTintColor = UIColor.white
    }
}
