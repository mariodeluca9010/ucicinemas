//
//  PlusViewController.swift
//  Uciapp
//
//  Created by Mario De luca on 13/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit

class PlusViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }

}

extension PlusViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlusCellTableViewCell", for: indexPath) as? PlusCellTableViewCell else {
            fatalError("Error")
        }
        return cell
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 970
    }
    
}

