//
//  Prox.swift
//  Uciapp
//
//  Created by Mario De luca on 11/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit

class Prox: UITableViewCell {

    var images3 = [UIImage(named: "poster4"),UIImage(named: "poster5"),UIImage(named: "poster6"),UIImage(named: "poster9"),UIImage(named: "poster10"),UIImage(named: "poster11")]
    
    
    @IBOutlet weak var collectionview3: UICollectionView!
    
    override func awakeFromNib() {
           super.awakeFromNib()
               // Initialization code
               
           collectionview3.delegate = self as UICollectionViewDelegate
           collectionview3.dataSource = self as UICollectionViewDataSource
                          }

                          override func setSelected(_ selected: Bool, animated: Bool) {
                          super.setSelected(selected, animated: animated)

                          // Configure the view for the selected state
                          }

                          }

    extension Prox : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return images3.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell3 = collectionview3.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath) as! CollectionViewCell3
            cell3.images3.image = images3[indexPath.row]
            
            return cell3
        }
    //
    //                      func collectionView2(_ collectionView2: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //                          return images2.count
    //                      }
                  

    //                          func collectionView2(_ collectionView2: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    //                              let cell2 = collectionView2.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! CollectionViewCell2
    //                              cell2.images2.image = images2[indexPath.row]
    //
    //                              return cell2
    //
    //                          }
                              
                              
                              //funzione che setta la size della cella
             func collectionView(_ collectionView2: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                                  return CGSize(width: 141, height: 235)
                              }
                              
}
