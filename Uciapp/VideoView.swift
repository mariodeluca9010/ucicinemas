//
//  VideoView.swift
//  Uciapp
//
//  Created by Mario De luca on 13/12/2019.
//  Copyright © 2019 Mario De luca. All rights reserved.
//

import UIKit
import AVKit

class VideoView: UIViewController {

    @IBAction func Play(_ sender: Any) {
        
        if let path = Bundle.main.path(forResource: "StarWars", ofType: "mp4"){
            let video = AVPlayer(url: URL(fileURLWithPath: path))
            let videoPlayer = AVPlayerViewController()
            videoPlayer.player = video
            
            present(videoPlayer, animated: false, completion:{
                video.play()
            })
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.popViewController(animated: true)

        dismiss(animated: true, completion: nil)
        overrideUserInterfaceStyle = .light
        
         
        // Do any additional setup after loading the view.
    }
    

    @IBAction func hide(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}



 
   extension VideoView : UITableViewDelegate, UITableViewDataSource{
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 1
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           guard let cell = tableView.dequeueReusableCell(withIdentifier: "Videocell", for: indexPath) as? Videocell else {
               fatalError("Error")
           }
           return cell
       }
       

       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 901
       }
       
   }

